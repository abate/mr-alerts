import gspread
from gitlab import Gitlab
from datetime import datetime, timedelta
from decouple import config, UndefinedValueError
from gspread_formatting import format_cell_range, set_column_width, cellFormat
import argparse
import tempfile
import os

DEFAULT_SPREADSHEET = "mr-alerts"
current_sheet_name = "current"
archived_sheet_name = "archived"
filled_row_count = 0


def next_available_row(worksheet):
    str_list = list(filter(None, worksheet.col_values(1)))
    return len(str_list) + 1


headers = [
    ("MR IID", "hidden"),
    ("MR URL", "url"),
    ("Title", "string"),
    ("Milestone", "url"),
    ("Author", "string"),
    ("Assignees", "string"),
    ("Creation Date", "string"),
    ("Updated", "hidden"),
    ("Updated last", "string"),
    ("Comments", "string"),
]


# https://stackoverflow.com/questions/67977884/python-gspread-hyperlink-adds
# https://docs.gitlab.com/ee/api/merge_requests.html
def format_row(mr):
    assignees = ", ".join([reviewer["name"] for reviewer in mr.assignees])
    created_at = datetime.strptime(mr.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    updated_at = datetime.strptime(mr.updated_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    return [
        str(mr.iid),
        (mr.web_url, mr.references["short"]),
        mr.title,
        (mr.milestone["web_url"], mr.milestone["title"]),
        mr.author["name"],
        assignees,
        created_at.strftime("%Y-%m-%d"),
        mr.updated_at,
        updated_at.strftime("%Y-%m-%d"),
    ]


def column_index(name):
    for index, (header_name, _) in enumerate(headers):
        if header_name == name:
            return index
    return -1  # Name not found


def column_letter(name):
    index = column_index(name)
    if 0 <= index < 26:
        return chr(ord("A") + index)
    else:
        return None


def style_sheet(worksheet):
    print("Add Header, hide id column")
    worksheet.append_row([item[0] for item in headers], 1)
    worksheet.freeze(1)  # index of the row
    for label, type in headers:
        idx = column_index(label)
        if type == "hidden":
            worksheet.hide_columns(idx, idx + 1)

    fmt = cellFormat(horizontalAlignment="LEFT")

    format_cell_range(
        worksheet, f"{column_letter('MR IID')}:{column_letter('Updated last')}", fmt
    )
    set_column_width(worksheet, column_letter("Title"), 400)
    set_column_width(worksheet, column_letter("Author"), 200)
    set_column_width(worksheet, column_letter("Milestone"), 200)
    set_column_width(worksheet, column_letter("Assignees"), 200)
    set_column_width(worksheet, column_letter("Comments"), 300)


def compute_values(data, lagging):
    values = []
    for type, value in [(h[1], r) for h, r in zip(headers[:-1], data)]:
        if type == "url":
            (url, text) = value
            v = {
                "userEnteredValue": {"stringValue": text},
                "textFormatRuns": [{"format": {"link": {"uri": url}}}],
            }
        else:
            v = {"userEnteredValue": {"stringValue": value}}
        if lagging:
            v["userEnteredFormat"] = {
                "backgroundColor": {"red": "1", "green": "0.4", "blue": "0.4"},
            }
        values.append(v)

    return values


# create the append request
def update_row_(worksheet, data, row_number, lagging):
    values = compute_values(data, lagging)

    request = {
        "updateCells": {
            "rows": [{"values": values}],
            "range": {
                "sheetId": worksheet.id,
                "startRowIndex": row_number,
                "endRowIndex": row_number + 1,
                "endColumnIndex": len(values),
            },
            "fields": "userEnteredValue, textFormatRuns, userEnteredFormat",
        }
    }

    return [request]


# create the append request
def add_row_(worksheet, data, lagging):
    values = compute_values(data, lagging)

    global filled_row_count
    filled_row_count = filled_row_count + 1

    # Create a request to append the new row
    # rowIndex starts from 0
    append_request = {
        "updateCells": {
            "rows": [{"values": values}],
            "start": {"sheetId": worksheet.id, "rowIndex": filled_row_count},
            "fields": "userEnteredValue, textFormatRuns, userEnteredFormat",
        }
    }

    return [append_request]


def nice_days(time):
    time = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")
    delta = (datetime.now() - time).days
    if delta == 0:
        return "today"
    elif delta == 1:
        return "yesterday"
    else:
        return f"{delta} days ago"


def is_valid_merge_request(mr, days, exclude_assignee, max_age_days):
    created_at = datetime.strptime(mr.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    days_ago = datetime.now() - timedelta(days=days)
    stale_threshold = datetime.now() - timedelta(days=max_age_days)

    # Define the conditions
    conditions = [
        created_at < days_ago,  # open for at least days
        created_at >= stale_threshold,  # Not older than max_age_days
        not mr.draft,
        not mr.work_in_progress,
        mr.milestone is not None and not mr.milestone.get("expired", False),
        all(assignee["username"] != exclude_assignee for assignee in mr.assignees),
    ]

    # Check if all conditions are met
    return all(conditions)


def get_opened_merge_requests(
    project, days_ago, exclude_assignee="nomadic-margebot", max_age_days=30
):
    merge_requests = project.mergerequests.list(
        state="opened", get_all=True, target_branch="master"
    )
    return filter(
        lambda mr: is_valid_merge_request(mr, days_ago, exclude_assignee, max_age_days),
        merge_requests,
    )


def authenticate_gitlab():
    # Load environment variables from .env file with default values
    GITLAB_URL = config("GITLAB_URL", default="https://gitlab.com")
    try:
        GITLAB_PRIVATE_TOKEN = config("GITLAB_PRIVATE_TOKEN")
    except UndefinedValueError as e:
        raise EnvironmentError(f"Required environment variable is not defined: {e}")

    gl = Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)
    gl.auth()
    return gl


def authenticate_google_sheets(credentials_file):
    gc = gspread.service_account(filename=credentials_file)
    return gc


def find_row_by_value(search, values):
    res = None
    for i, row in enumerate(values):
        for _, c in enumerate(row):
            if str(search) in c:
                return (i, row)
    return res


def archive_mergerequests(project, current_sheet, archived_sheet, rows):
    for index, row in rows:
        iid = row[column_index("MR IID")]
        if iid.isdigit():
            mr = project.mergerequests.get(iid)
            print(f"Consering for archiving {mr.iid} {mr.state}")
            if mr.state == "merged" or mr.state == "closed":
                print(f"Archive row id {index}")
                current_sheet.delete_rows(index)
                archived_sheet.append_row(row)


# a mr is lagging if it was updated more than x days ago
def is_lagging(time, days):
    time = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")
    delta = (datetime.now() - time).days
    return delta >= days


def update_current_sheet(sh, project, current_sheet, archived_sheet, merge_requests):
    batch_append_request = []
    batch_update_request = []
    values = current_sheet.get_all_values()
    for mr in merge_requests:
        mr_info = format_row(mr)
        res = find_row_by_value(mr.iid, values)
        print(f"Consering for adding/updating {mr.iid} {mr.state}: ", end="")
        if res:
            (row_index, data) = res
            print("Updating.")
            idx = column_index("Updated last")
            mr_info.append(data[idx + 1 :])
            batch_update_request.append(
                update_row_(
                    current_sheet, mr_info, row_index, is_lagging(mr.updated_at, 7)
                )
            )
        else:
            print("Adding a new row.")
            mr_info.append("")
            batch_append_request.append(
                add_row_(current_sheet, mr_info, is_lagging(mr.updated_at, 7))
            )
    # archive closed/merged MRs
    archive_mergerequests(
        project,
        current_sheet,
        archived_sheet,
        [(i + 1, row) for i, row in enumerate(values) if i != 0],
    )
    # update old rows
    if len(batch_update_request) > 1:
        sh.batch_update({"requests": batch_update_request})
    # update old rows
    if len(batch_append_request) > 1:
        sh.batch_update({"requests": batch_append_request})


def delete_spreadsheet(gc, spreadsheet_title):
    try:
        # Find the spreadsheet by title or URL
        spreadsheet = gc.open(spreadsheet_title)

        # Use the Google Sheets API to delete the spreadsheet
        gc.del_spreadsheet(spreadsheet.id)
        del spreadsheet  # This removes the local reference to the spreadsheet
        print(f"Spreadsheet '{spreadsheet_title}' has been deleted.")
    except gspread.exceptions.SpreadsheetNotFound:
        print(f"Spreadsheet '{spreadsheet_title}' not found.")


def temp_file(env_variable_name):
    data = os.environ.get(env_variable_name)
    temp_file = None

    if data is not None:
        try:
            temp_file = tempfile.NamedTemporaryFile(mode="w", delete=False)
            temp_file.write(data)

            return temp_file.name

        except Exception as e:
            return f"An error occurred: {str(e)}"
        finally:
            if temp_file:
                temp_file.close()  # Ensure the temporary file is closed and deleted

    else:
        print("Environment variable not found.")
        exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--project-id", required=True, type=int, help="GitLab project ID"
    )
    parser.add_argument(
        "--days", type=int, default=3, help="Number of days to check for (default: 3)"
    )
    parser.add_argument(
        "--credentials-file", required=False, help="Google Sheets API credentials file"
    )
    parser.add_argument(
        "--spreadsheet",
        default=DEFAULT_SPREADSHEET,
        help="Title or URL of the spreadsheet to delete",
    )
    parser.add_argument("--delete", action="store_true", help="Delete the spreadsheet")
    parser.add_argument("--reset", action="store_true", help="Remove old data")

    args = parser.parse_args()

    if args.credentials_file:
        credentials_file = args.credentials_file
    else:
        credentials_file = temp_file("CREDENTIALS")

    gc = gspread.service_account(filename=credentials_file)

    if args.delete:
        delete_spreadsheet(gc, args.spreadsheet)
    else:
        gl = authenticate_gitlab()

        google_sheet_name = args.spreadsheet
        try:
            sh = gc.open(google_sheet_name)
        except gspread.exceptions.SpreadsheetNotFound:
            sh = gc.create(google_sheet_name)

        # accessible to all
        sh.share("", perm_type="anyone", role="writer")

        print(f"Sheet URL: {sh.url}")

        try:
            current_sheet = sh.worksheet(current_sheet_name)
        except gspread.exceptions.WorksheetNotFound:
            current_sheet = sh.add_worksheet(
                title=current_sheet_name, rows="500", cols="10"
            )
            style_sheet(current_sheet)
            # remove the default worksheet
            sh.del_worksheet(sh.worksheet("Sheet1"))

        try:
            archived_sheet = sh.worksheet(archived_sheet_name)
        except gspread.exceptions.WorksheetNotFound:
            archived_sheet = sh.add_worksheet(
                title=archived_sheet_name, rows="500", cols="10"
            )
            style_sheet(archived_sheet)

        project = gl.projects.get(args.project_id)
        current_merge_requests = get_opened_merge_requests(project, args.days)
        global filled_row_count
        # the the global variable here for the first time and update it
        # in the append_row function. This is needed to avoid exceeding
        # the 'Read requests per minute per user' quota limit
        filled_row_count = next_available_row(current_sheet)

        if args.reset:
            current_sheet.clear()
            style_sheet(current_sheet)

        update_current_sheet(
            sh, project, current_sheet, archived_sheet, current_merge_requests
        )

        current_sheet.add_protected_range(
            f"{column_letter('MR IID')}:{column_letter('Updated last')}",
            description="Rows managed by a robot",
            editor_users_emails=["pietro@lambda-coins.com"],
        )

        current_sheet.sort(
            (column_index("Milestone"), "asc"), (column_index("Updated"), "des")
        )
        print(f"Sheet URL: {sh.url}")


if __name__ == "__main__":
    main()
