
import requests
import json
from datetime import datetime, timedelta
import argparse
import yaml
from decouple import config
import gitlab
import os
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
# add this to activate debugging
# import logging
# logging.basicConfig(level=logging.DEBUG)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Check GitLab MRs and send Slack notifications.')
    parser.add_argument('--project-id', required=True,
                        help='GitLab project ID')
    parser.add_argument('--slack-channel', required=False,
                        help='Default slack channel for alerts')
    parser.add_argument('--days', type=int, default=3,
                        help='Number of days to check for (default: 3)')
    parser.add_argument('--dry-run', action='store_true',
                        help='Perform a dry run without sending Slack notifications')
    parser.add_argument('--config', type=str, required=True,
                        help='Path to team/ slack channel list')
    return parser.parse_args()


def find_team_and_channel(username, config, default_slack_channel):
    if config is not None and 'teams' in config:
        for team in config.get('teams', []):
            members = team.get('members', [])
            for member in members:
                if member.get('username') == username:
                    return {
                        'name': team.get('name'),
                        'channel': team.get('channel')
                    }
    return {'name': "default", 'channel': default_slack_channel}


def send_slack_message(slack_token, default_slack_channel, config, message, username):

    client = WebClient(token=slack_token)

    team = find_team_and_channel(username, config, default_slack_channel)
    channel = team['channel']
    print(channel)

    try:
        response = client.chat_postMessage(
            channel=channel,
            text=message
        )
        print(
            f"Notification sent to Slack ({channel})")
        print(message)
        return response
    except SlackApiError as e:
        print(
            f"Failed to send notification to Slack ({channel})")
        print(message)
        print(e.response["error"])
        return


def prepare_message(template, values):
    template_directory = config('TEMPLATES', default="templates")

    template_file = os.path.join(template_directory, template)

    if not os.path.isfile(template_file):
        print(f"Template file '{template_file}' not found.")
    else:
        with open(template_file, "r", encoding="utf-8") as file:
            template_content = file.read()

        return template_content.format(**values)


def nice_days(time):

    delta = (datetime.now() - time).days

    if delta == 0:
        return "today"
    elif delta == 1:
        return "yesterday"
    else:
        return f"{delta} days ago"

# Main function


def is_valid_merge_request(mr, days_ago, exclude_assignee='nomadic-margebot', max_age_days=30):
    created_at = datetime.strptime(mr.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
    stale_threshold = (datetime.now() - timedelta(days=max_age_days))

    # Define the conditions
    conditions = [
        created_at < days_ago,
        created_at >= stale_threshold,  # Not older than 30 days
        not mr.draft,
        mr.milestone is not None and not mr.milestone.get('expired', False),
        all(assignee['username'] !=
            exclude_assignee for assignee in mr.assignees)
    ]

    # Check if all conditions are met
    return all(conditions)


def main():
    args = parse_arguments()

    try:
        with open(args.config, 'r') as file:
            configfile = yaml.safe_load(file)
    except FileNotFoundError:
        print(f"Error: File not found - {args.config}")
        exit(1)
    except yaml.YAMLError as e:
        print(f"Error parsing YAML: {e}")
        exit(1)

    if args.slack_channel is None and configfile is None:
        print(
            "Error: You must specify the default slack_channel either on the command line or in the config file")
        exit(1)

    # Load environment variables from .env file with default values
    GITLAB_URL = config('GITLAB_URL', default='https://gitlab.com')
    try:
        GITLAB_PRIVATE_TOKEN = config('GITLAB_PRIVATE_TOKEN')
    except decouple.UndefinedValueError as e:
        raise EnvironmentError(
            f"Required environment variable is not defined: {e}")
    try:
        SLACK_API_TOKEN = config('SLACK_API_TOKEN')
    except decouple.UndefinedValueError as e:
        if not args.dry_run:
            raise EnvironmentError(
                f"Required environment variable is not defined: {e}")

    # Initialize the GitLab client
    gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)

    gl.auth()

    # Calculate the date 'days' days ago from today
    days_ago = datetime.now() - timedelta(days=args.days)

    # Get the project by ID
    project_id = args.project_id
    project = gl.projects.get(project_id)

    # Fetch open merge requests
    merge_requests = project.mergerequests.list(
        state='opened', get_all=True, order_by="created_at", sort="asc")

    for mr in merge_requests:
        created_at = datetime.strptime(mr.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        if is_valid_merge_request(mr, days_ago):
            reviewers = [reviewer['name'] for reviewer in mr.reviewers]
            assignees = [reviewer['name'] for reviewer in mr.assignees]
            updated_at = datetime.strptime(
                mr.updated_at, '%Y-%m-%dT%H:%M:%S.%fZ')
            values = {
                "title": mr.title,
                "days": args.days,
                "days_ago": (datetime.now() - created_at).days,
                "author": mr.author['name'],
                "assignees": ', '.join(assignees),
                "reviewers": ', '.join(reviewers),
                "comments": mr.user_notes_count,
                "last_activity": nice_days(updated_at),
                "milestone": mr.milestone['title'],
                "milestone_url": mr.milestone['web_url'],
                "url": mr.web_url,
                "number": mr.references['short']
            }
            message = prepare_message("late_merge_request.md", values)
            if args.slack_channel is None:
                default_slack_channel = configfile.get(
                    'default_slack_channel')
            else:
                default_slack_channel = args.slack_channel
            if not args.dry_run:
                send_slack_message(SLACK_API_TOKEN, default_slack_channel,
                                   configfile, message, mr.author['username'])
            else:
                team = find_team_and_channel(
                    mr.author['username'], configfile, default_slack_channel)
                print(
                    f"DRY_RUN: would send slack message for team {team['name']} on channel {team['channel']}")
                print(message)


if __name__ == '__main__':
    main()
