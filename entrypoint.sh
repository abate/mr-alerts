#!/bin/sh

PROJECT_ID=${PROJECT_ID:-3836952} # tezos/tezos
DAYS=${DAYS:-3}
INTERVAL_HOURS=${INTERVAL_HOURS:-12}

export GITLAB_PRIVATE_TOKEN="${GITLAB_PRIVATE_TOKEN:?}"
export SLACK_API_TOKEN="${SLACK_API_TOKEN:?}"
export TEMPLATES="${TEMPLATES:-"/app/templates/"}"

if [ ${SLACK_CHANNEL+x} ]; then
  default_slack_channel="--slack-channel ${SLACK_CHANNEL}"
fi

if [ ${default_slack_channel+x} ] && [ ${config_file+x} ]; then
  echo "either the --config argument or the --slack-channel must be specified"
  exit 1
fi

echo "Send alerts on project ${PROJECT_ID} $default_slack_channel"

#shellcheck disable=SC2086
while true; do
   python /app/alert-mr.py  \
    --project-id "${PROJECT_ID}" \
    --days "$DAYS" \
    --config /app/config.yaml \
    $default_slack_channel "$@"
  echo "Now going to sleep for ${INTERVAL_HOURS}h. ZZZzzzz"
  sleep "${INTERVAL_HOURS}h"
done

