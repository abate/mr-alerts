# Merge Requests Alerts for Tezos/Tezos

This project consists of a simple Python script, a Dockerfile, and a Helm chart
for deployment. Follow these steps to build and deploy the project:

## Prerequisites

Before you begin, ensure you have the following tools and dependencies
installed:

- Docker
- Docker Compose
- Python (for local development)
- Poetry (for Python package management)
- Helm (for Kubernetes deployment)

## Building and Testing the Docker Image Locally

The Docker Compose commands below build and run a Docker image using the Python
script.

1. Ensure you are in the project directory:

    ```shell
    docker-compose build
    docker-compose up
    ```

## Configure

It is possible to configure this micro service by passing a configuration file

### Ex.
```
default_slack_channel: "merge-request-alerts"
teams:
  - name: infrastruture
    channel: "#slack-channel-infra"
    em: joe
    members:
      - username: marc
      - username: michoux
  - name: l1
    channel: "#slack-channel-l1"
    em: michel
    members:
      - username: joel
      - username: guadalupe
```

For each MR, instead of sending the alert to the default channel, we fetch the
author and associate it to a team, and a dedicated slack channel.


## Deployment

For Helm deployment, make sure you have a working Kubernetes cluster.

You can use all the default values in `helm/values.yaml`, but you must define a
file with your GitLab and Slack credentials in `helm/secret-values.yml`.

This file has the following format:

```yaml
secrets:
  gitlabPrivateToken: "glpat-XXXXXXXXXXXXX"
  slackApiToken: "xoxb-XXXXXXXXXXXXXXXXX" # oAuth
```

For the initial deployment:

   helm install mr-alert helm/ -f helm/secret-values.yml

To upgrade:

   helm upgrade mr-alert helm/ -f helm/secret-values.yml

To remove the deployment:

    helm uninstall mr-alert
